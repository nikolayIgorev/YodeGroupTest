[System.Serializable]
public class Book
{
    public string Author;
    public string Genre;
    public string Name;
    public string PublicationDate;
}
