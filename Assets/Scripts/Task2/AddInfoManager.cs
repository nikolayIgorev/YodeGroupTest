using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddInfoManager : MonoBehaviour
{
    static AddInfoManager instance;
    [SerializeField] Transform addInfoPanel;
    TextMeshProUGUI genreText;
    TextMeshProUGUI dateText;
    void Start()
    {
        instance = this;
        genreText = addInfoPanel.GetChild(0).GetComponent<TextMeshProUGUI>();
        dateText = addInfoPanel.GetChild(1).GetComponent<TextMeshProUGUI>();
    }
    public static void ShowInfo(string g, string d)
    {
        instance.genreText.text = g;
        instance.dateText.text = (d == " ") ? "--" : d;
        instance.addInfoPanel.gameObject.SetActive(true);
    }
}
