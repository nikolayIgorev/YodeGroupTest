using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsManager : MonoBehaviour
{
    [SerializeField] Button allButton;
    [SerializeField] Button favouriteButton;
    [SerializeField] GameObject allScroll;
    [SerializeField] GameObject favouriteScroll;
    void Start()
    {
        allButton.onClick.AddListener(EnableAll);
        favouriteButton.onClick.AddListener(EnableFavourite);
    }
    void EnableAll()
    {
        allScroll.SetActive(true);
        favouriteScroll.SetActive(false);
        allButton.interactable = false;
        favouriteButton.interactable = true;
    }
    void EnableFavourite()
    {
        favouriteScroll.SetActive(true);
        allScroll.SetActive(false);
        favouriteButton.interactable = false;
        allButton.interactable = true;
    }
}
