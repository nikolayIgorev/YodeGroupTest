using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class BooksLoader : MonoBehaviour
{
    [SerializeField] GameObject checkConnectionText;
    [SerializeField] GameObject bookPrefab;
    [SerializeField] Transform scrollView;
    string url = "https://raw.githubusercontent.com/AkimZay/CollectionJSON/master/Books.json";
    float timer = 0.0f;
    bool requestDone = false;
    async void Start()
    {
        while(!requestDone)
        {
            using var uwr = UnityWebRequest.Get(url);
            var operation = uwr.SendWebRequest();
            while(!operation.isDone)
            {
                await Task.Yield();
                timer += Time.deltaTime;
                if(timer > 1.0f && !checkConnectionText.activeSelf)
                {
                    checkConnectionText.SetActive(true);
                }
            }
            if(uwr.result == UnityWebRequest.Result.Success)
            {
                CreateBooks("{\"Items\":" + uwr.downloadHandler.text + "}");
                requestDone = true;
                checkConnectionText.SetActive(false);
            }
        }
    }
    void CreateBooks(string json)
    {
        Book[] books = JsonHelper.FromJson<Book>(json);
        for(int i = 0; i < books.Length; i++)
        {
            GameObject book = Instantiate(bookPrefab, Vector2.zero, Quaternion.identity, scrollView);
            book.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = books[i].Author;
            book.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = books[i].Name;
            book.AddComponent<BookData>().SetFields(books[i], i);
        }
    }
}
