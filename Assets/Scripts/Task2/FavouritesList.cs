using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FavouritesList : MonoBehaviour
{
    static FavouritesList instance;
    [SerializeField] Transform favScrollView;
    [SerializeField] Transform allScrollView;
    [SerializeField] GameObject bookPrefab;
    static List<BookData> lovedBooks;
    void Start()
    {
        instance = this;
        lovedBooks = new List<BookData>();
    }
    public static void AddBook(BookData newBook)
    {
        for(int i = 0; i < lovedBooks.Count; i++)
        {
            if(lovedBooks[i].id == newBook.id) return;
        }
        lovedBooks.Add(newBook);
        GameObject book = Instantiate(instance.bookPrefab, Vector2.zero, Quaternion.identity, instance.favScrollView);
        book.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = newBook.bookEntity.Author;
        book.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = newBook.bookEntity.Name;
        book.AddComponent<BookData>().SetFields(newBook.bookEntity, newBook.id);
    }
    public static void RemoveBook(BookData oldBook)
    {
        lovedBooks.Remove(oldBook);
        for(int i = 0; i < instance.favScrollView.childCount; i++)
        {
            if(instance.favScrollView.GetChild(i).GetComponent<BookData>().id == oldBook.id)
            {
                Destroy(instance.favScrollView.GetChild(i).gameObject);
            }
        }
        instance.allScrollView.GetChild(oldBook.id).GetComponent<BookData>().ResetHeartColour();
    }
}
