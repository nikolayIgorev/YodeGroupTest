using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BookData : MonoBehaviour, IPointerClickHandler
{
    Button heartButton;
    Image heartButtonImage;
    public Book bookEntity { get; private set; }
    public int id { get; private set; }
    bool isLoved;
    void Start()
    {
        heartButton = GetComponentInChildren<Button>();
        heartButtonImage = heartButton.transform.GetComponent<Image>();
        if(PlayerPrefs.HasKey("loved" + id))
        {
            FavouritesList.AddBook(this);
            heartButtonImage.color = Color.red;
            isLoved = true;
        }
        heartButton.onClick.AddListener(AddToLoved);
    }
    public void SetFields(Book bookInfo, int id)
    {
        bookEntity = bookInfo;
        this.id = id;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        AddInfoManager.ShowInfo(bookEntity.Genre, bookEntity.PublicationDate);
    }
    void AddToLoved()
    {
        if(isLoved)
        {
            FavouritesList.RemoveBook(this);
            heartButtonImage.color = Color.white;
            PlayerPrefs.DeleteKey("loved" + id);
        }
        else
        {
            FavouritesList.AddBook(this);
            heartButtonImage.color = Color.red;
            PlayerPrefs.SetString("loved" + id, "yes");
        }
        isLoved = !isLoved;
        PlayerPrefs.Save();
    }
    public void ResetHeartColour()
    {
        heartButtonImage.color = Color.white;
    }
}
