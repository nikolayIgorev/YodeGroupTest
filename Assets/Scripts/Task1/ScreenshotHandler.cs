using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotHandler : MonoBehaviour
{
    [SerializeField] GameObject blinkPrefab;
    [SerializeField] Canvas canvas;
    public void TakeScreenshot()
    {
        StartCoroutine(Screenshot());
    }
    IEnumerator Screenshot()
    {
        AudioManager.PlaySound(AudioManager.Sound.Screenshot);
        string fileName = "Screenshot" + System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".png";
        ScreenCapture.CaptureScreenshot(fileName);
        yield return new WaitForEndOfFrame();
        GameObject blink = Instantiate(blinkPrefab, Vector2.zero, Quaternion.identity, canvas.transform);
        blink.transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(0.0f, 0.0f);
    }
}
