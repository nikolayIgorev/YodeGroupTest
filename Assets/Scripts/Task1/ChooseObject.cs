using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseObject : MonoBehaviour
{
    Button button;
    string objectName;
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ChangeObject);
        objectName = gameObject.name;
    }
    void ChangeObject()
    {
        StartCoroutine(LoadAssetBundle.Load(objectName));
        AudioManager.PlaySound(AudioManager.Sound.Select);
    }
}
