using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ProgrammManager : MonoBehaviour
{
    [SerializeField] GameObject planeMarker;
    GameObject objectToSpawn;
    GameObject selectedObject;
    GUIController guiController;
    ARRaycastManager arRaycastManager;
    List<ARRaycastHit> hits;
    Vector2 screenCenter;
    float rotSensitivity = 0.2f;
    float touchesRange, prevTouchesRange;
    float maxScale = 1.0f, minScale = 0.1f;
    bool chooseObject;
    bool rotation;
    void Start()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
        guiController = GetComponent<GUIController>();
        hits = new List<ARRaycastHit>();
        objectToSpawn = new GameObject();
        screenCenter = new Vector2(Screen.width / 2.0f, Screen.height / 2.0f);
        planeMarker.SetActive(false);
    }
    void Update()
    {
        if(chooseObject)
        {
            ShowMarkerAndSetObject();
        }
        Interact();
    }
    public void SetObjectToSpawn(GameObject value)
    {
        if(!objectToSpawn.Equals(value) && value != null)
        {
            objectToSpawn = value;
        }
        guiController.ShowScrollView(false);
        chooseObject = true;
    }
    public void ChangeInteractionWay()
    {
        rotation = !rotation;
    }
    void ShowMarkerAndSetObject()
    {
        if(hits.Count > 0)
        {
            hits.Clear();
        }
        arRaycastManager.Raycast(screenCenter, hits, TrackableType.Planes);
        if(hits.Count > 0)
        {
            planeMarker.transform.position = hits[0].pose.position;
            planeMarker.SetActive(true);
        }
        if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Instantiate(objectToSpawn, hits[0].pose.position, objectToSpawn.transform.rotation);
            chooseObject = false;
            planeMarker.SetActive(false);
            guiController.ShowTapToPlaceText(false);
        }
    }
    void Interact()
    {
        if(Input.touchCount < 1)
        {
            return;
        }
        Touch touch = Input.GetTouch(0);
        if(touch.phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                AudioManager.PlaySound(AudioManager.Sound.Tap);
                if(!hit.collider.CompareTag("Selected"))
                {
                    hit.collider.gameObject.tag = "Selected";
                }
                selectedObject = GameObject.FindWithTag("Selected");
                guiController.ShowInfo(selectedObject.name.Replace("(Clone)", string.Empty));
            }
        }
        if(touch.phase == TouchPhase.Moved && Input.touchCount == 1)
        {
            if(rotation)
            {
                selectedObject.transform.rotation *= Quaternion.Euler(0.0f, -touch.deltaPosition.x * rotSensitivity, 0.0f);
            }
            else
            {
                arRaycastManager.Raycast(touch.position, hits, TrackableType.Planes);
                selectedObject.transform.position = hits[0].pose.position;
            }
        }
        if(Input.touchCount == 2)
        {
            ChangeScale();
        }
        if(touch.phase == TouchPhase.Ended)
        {
            if(selectedObject.CompareTag("Selected"))
            {
                selectedObject.tag = "Unselected";
            }
        }
    }
    void ChangeScale()
    {
        Touch touch = Input.GetTouch(0);
        Touch touch1 = Input.GetTouch(1);
        if(touch.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Moved)
        {
            touchesRange = Vector2.Distance(touch.position, touch1.position);
            prevTouchesRange = Vector2.Distance(touch.position - touch.deltaPosition, touch1.position - touch1.deltaPosition);
            if((touchesRange - prevTouchesRange) > 0.0f)
            {
                if(Vector3.SqrMagnitude(selectedObject.transform.localScale) < maxScale)
                {
                    selectedObject.transform.localScale += Vector3.one * Time.deltaTime;
                }
            }
            else
            {
                if(Vector3.SqrMagnitude(selectedObject.transform.localScale) > minScale)
                {
                    selectedObject.transform.localScale -= Vector3.one * Time.deltaTime;
                }
            }
        }
    }
}
