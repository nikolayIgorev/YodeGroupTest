using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionChanger : MonoBehaviour
{
    ProgrammManager programmManager;
    Button button;
    [SerializeField] Button anotherButton;
    void Start()
    {
        programmManager = FindObjectOfType<ProgrammManager>();
        button = GetComponent<Button>();
        button.onClick.AddListener(ChangeState);
    }
    void ChangeState()
    {
        programmManager.ChangeInteractionWay();
        gameObject.SetActive(false);
        anotherButton.gameObject.SetActive(true);
    }
}
