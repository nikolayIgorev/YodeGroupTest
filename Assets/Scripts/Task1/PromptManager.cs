using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PromptManager : MonoBehaviour
{
    TextMeshProUGUI text;
    Button nextButton;
    int index = 0;
    List<string> phrases;
    void Start()
    {
        text = GetComponentInChildren<TextMeshProUGUI>();
        nextButton = GetComponentInChildren<Button>();
        nextButton.onClick.AddListener(NextPhrase);
        phrases = new List<string>()
        {
            "Press \"Add new object\"\nto select model",
            "Point your device\nto a surface",
            "Tap to create object",
            "Use fingers to interact\nwith object"
        };
        text.text = phrases[index];
    }
    void NextPhrase()
    {
        ++index;
        if(index >= phrases.Count)
        {
            Destroy(gameObject);
            return;
        }
        text.text = phrases[index];
    }
}
