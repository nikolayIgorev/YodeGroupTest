using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

public class LoadAssetBundle : MonoBehaviour
{
    static LoadAssetBundle instance;
    AssetBundle bundle;
    GameObject prefab;
    ProgrammManager programmManager;
    void Start()
    {
        instance = this;
        programmManager = GetComponent<ProgrammManager>();
    }
    public static IEnumerator Load(string name)
    {
        if(instance.bundle == null)
        {
            string path = Path.Combine(Application.streamingAssetsPath, "content/voxelanimals");
            AssetBundleCreateRequest createRequest = AssetBundle.LoadFromFileAsync(path);
            yield return createRequest;
            instance.bundle = createRequest.assetBundle;
            instance.prefab = instance.bundle.LoadAsset<GameObject>(name);
        }
        else
        {
            instance.prefab = instance.bundle.LoadAsset<GameObject>(name);
        }
        instance.programmManager.SetObjectToSpawn(instance.prefab);
    }
}
