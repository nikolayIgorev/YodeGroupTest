using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public enum Sound
    {
        Tap,
        Select,
        Screenshot
    }
    static AudioManager instance;
    AudioSource source;
    [SerializeField] AudioClip[] sounds;
    void Start()
    {
        instance = this;
        source = GetComponent<AudioSource>();
    }
    public static void PlaySound(Sound soundType)
    {
        instance.source.clip = instance.sounds[(int)soundType];
        instance.source.Play();
    }
}
