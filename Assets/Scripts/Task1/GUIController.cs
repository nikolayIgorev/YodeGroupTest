using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GUIController : MonoBehaviour
{
    [SerializeField] GameObject scrollView;
    [SerializeField] GameObject infoPanel;
    Image infoImage;
    [SerializeField] TextMeshProUGUI tapToPlaceText;
    TextMeshProUGUI infoText;
    bool isShowInfo;
    void Start()
    {
        infoImage = infoPanel.GetComponent<Image>();
        infoText = infoPanel.GetComponentInChildren<TextMeshProUGUI>();
        infoImage.color = new Color(infoImage.color.r, infoImage.color.g, infoImage.color.b, 0.0f);
        infoText.color = new Color(infoText.color.r, infoText.color.g, infoText.color.b, 0.0f);
    }
    public void ShowScrollView(bool value)
    {
        scrollView.SetActive(value);
        ShowTapToPlaceText(!value);
    }
    public void ShowTapToPlaceText(bool value)
    {
        tapToPlaceText.gameObject.SetActive(value);
    }
    public void ShowInfo(string name)
    {
        if(isShowInfo) return;
        infoText.text = name;
        StartCoroutine(SmoothInfoEnabling());
    }
    IEnumerator SmoothInfoEnabling()
    {
        isShowInfo = true;
        infoPanel.SetActive(true);
        float currentAlpha = 0.0f;
        while(currentAlpha <= 1.0f)
        {
            currentAlpha += Time.deltaTime;
            infoImage.color = new Color(infoImage.color.r, infoImage.color.g, infoImage.color.b, currentAlpha);
            infoText.color = new Color(infoText.color.r, infoText.color.g, infoText.color.b, currentAlpha);
            yield return null;
        }
        yield return new WaitForSeconds(1.0f);
        currentAlpha = 1.0f;
        while(currentAlpha >= 0.0f)
        {
            currentAlpha -= Time.deltaTime;
            infoImage.color = new Color(infoImage.color.r, infoImage.color.g, infoImage.color.b, currentAlpha);
            infoText.color = new Color(infoText.color.r, infoText.color.g, infoText.color.b, currentAlpha);
            yield return null;
        }
        infoPanel.SetActive(false);
        isShowInfo = false;
    }
}
