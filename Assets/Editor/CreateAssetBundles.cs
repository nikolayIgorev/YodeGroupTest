using UnityEditor;

public class CreateAssetBundles
{
    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        BuildPipeline.BuildAssetBundles(UnityEngine.Application.streamingAssetsPath, BuildAssetBundleOptions.None, BuildTarget.Android);
    }
}